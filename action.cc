#include "board.h"

using namespace std;

bool Board::validLoc(vector<Pos> target) {
    for (int i=0; i<4; ++i) {
        Pos p = target[i];
        if ((p.row < 0 || p.row > 17 || p.col < 0 || p.col > 10) ||
            (theGrid[p.row][p.col].id != 0 && 
             theGrid[p.row][p.col].id!=blockCounter)) {
                 return false;
        }
    }
    return true;
}

void Board::clearBlock(vector<Pos> p) {
    for (int i=0; i<4; ++i) {
        theGrid[p[i].row][p[i].col].id = 0;
        td->notify(theGrid[p[i].row][p[i].col]);
    }
}

bool Board::move(Direction d) {
    Pos restore = anchor;

    switch (d) {
        case Direction::L: anchor = Pos{anchor.row, anchor.col-1}; break;
        case Direction::R: anchor = Pos{anchor.row, anchor.col+1}; break;
        case Direction::D: anchor = Pos{anchor.row+1, anchor.col}; break;
    }

    vector<Pos> target = eval(anchor, posIndex, type); 
    if (!validLoc(target)) {
        //std::cout << "invalid LOC ---------------------------------" << std::endl;
        anchor = restore;
        return false;
    } else {
        clearBlock(loc);
        //cout << "clearing block in move" << endl;
        loc = target;
        //cout << bock<< endl;
        update();
        return true;
    }
}


void Board::rotate(Rotation r) {
    int restore = posIndex;
    switch (type) {

        case Type::I:
        case Type::S:
        case Type::Z:
            if (posIndex == 0) posIndex = 1;
            else posIndex = 0;
            break;

        case Type::J:
        case Type::L:
        case Type::T:

            switch (r) {
                case Rotation::CW:
                    if (posIndex == 3) posIndex = 0;
                    else posIndex++;
                    break;  

                case Rotation::CCW:
                    if (posIndex == 0) posIndex = 3;
                    else posIndex--;
                    break;
            }
            break;  
 
	default:
	    break;
    }

    vector<Pos> target = eval(anchor, posIndex, type); 
    if (!validLoc(target)) 
        posIndex = restore;
    else {
        clearBlock(loc);
        loc = target;
        update();
    }
}

void Board::drop() {
	vector<Pos> temp = loc;
    bool print = true;
    while (move(Direction::D)) {}       //why is the move loc the same
    for (int i = 0; i < 4; ++i){
        if (temp[i].row!=loc[i].row || temp[i].col!=loc[i].col) print=false;
    }
    if (print) gd->updateBlock(loc, color);
	dropped=true;
	update();
	dropped=false;    
    newBlock();
}

