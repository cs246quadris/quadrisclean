#ifndef _GRAPHICAL_H_
#define _GRAPHICAL_H_

#include <iostream>
#include <iomanip>
#include <vector>
using std::vector;

#include "window.h"
#include "util.h"
#include "board.h"
#include "type.h"
#include "cell.h"

class GraphicalDisplay {
	const int rowNum = 18;
	const int colNum = 11;
	const int cellSize = 30;                                // divide by two in smaller screen
    
    const int bw = 10;  // border width                     // divide by two in smaller screen
    const int cw = 2;   // cell in-between width            // divide by two in smaller screen
    const int tr = 25;  // total row
    const int tc = 17;
    // bool bestMove = false;  //display bestMove = false;
    bool graphicOn = false;
    int clrScheme;
    
	Xwindow *window;
  	std::vector<std::vector<int>> nextBlock;

public:
    GraphicalDisplay(bool graphicOn, int clrScheme, int lvl, int hiScore);            //changed
  	void notify(Cell c);
    void clear(Cell c);
  	void setNext(Type t);
    
    void updateBlock(vector<Pos> loc, int curColour);
    void updateHi(int s);
    void updateScore(int s);
    void updateLevel(int s);
    
    void showBestMove(vector<Pos> loc);
    ~GraphicalDisplay(){delete window;}
};

#endif





