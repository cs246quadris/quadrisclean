#include "board.h"
using namespace std;

Type Board::getTypeFromSeq() {
    Type t;
    if (sequence.size() == 0) {
        vector<char> temp = {'I', 'L', 'O', 'T', 'Z', 'S', 'J'};
        sequence = std::move(temp);
    }
        t = charToType(sequence[(seqInd++) % sequence.size()]);
    
    return t;
}

void Board::setNextType() {
    Type L1[12] = {Type::S, Type::Z, Type::I, Type::I, Type::L, Type::L,
                     Type::O, Type::O, Type::T, Type::T, Type::J, Type::J};
    Type L2[7] = {Type::S, Type::Z, Type::I, Type::L, Type::O, Type::T, Type::J};
    Type L3[9] = {Type::S, Type::S, Type::Z, Type::Z, Type::I, Type::L, 
                     Type::O, Type::T, Type::J};
    switch(level) {
        case 0:
            nextType = getTypeFromSeq(); break;
        case 1:
            nextType = L1[rand() % 12]; break;
        case 2:
            nextType = L2[rand() % 7]; break;
        default:
            nextType = (random ? L3[rand() % 9] : getTypeFromSeq()); 
    }
}




