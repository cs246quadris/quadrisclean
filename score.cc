#include "board.h"

void Board::updateCurScoreRow(int cleared) {
    curScore += (cleared + level) * (cleared + level);
    gd->updateScore(curScore);
}

void Board::updateCurScoreBlock(int level_birth) {
    curScore += (level_birth + 1) * (level_birth + 1);
    gd->updateScore(curScore);
}

void Board::updateHighScore() {
    if (curScore > hiScore) {
        hiScore = curScore;
        gd->updateHi(hiScore);
    }
}



