#include "board.h"
using namespace std;

bool Board::atBottom() {
    for (int i=0; i<4; ++i) {
        if (loc[i].row == 17-colFilled[loc[i].col]) {
            return true;
        }
    }
    return false;
}

bool Board::updateTheGrid() {
    //cout << "updateTheGrid In" << endl;
	
    for (int i=0; i<4; ++i) {
	if (loc[i].row<0 || loc[i].col<0 || loc[i].row>17 || loc[i].col>11) {
		gameOver = true;
		return false;
	}	
	cout << loc[i].row << " " << loc[i].col << endl;
        theGrid[loc[i].row][loc[i].col].id = blockCounter;
        theGrid[loc[i].row][loc[i].col].t = type;
        td->notify(theGrid[loc[i].row][loc[i].col]);
        // gd->notify(theGrid[loc[i].row][loc[i].col]);

    }
    //cout << "updateTheGrid Out" << endl;
	return true;

}

void Board::updateRowCol() {
    //cout << "updateRowCol In" << endl;
	if (dropped) {
    	for (int i=0; i<4; ++i) {
        	if (18-loc[i].row > colFilled[loc[i].col])
            	colFilled[loc[i].col] = 18-loc[i].row;
        	++rowFilled[loc[i].row];
    	}
	}
    //cout << "updateRowCol Out" << endl;

}

void Board::checkFullRow(){
    int cleared = 0;
	if (dropped) {
    	for (int i=3; i<18; ++i) {
        	if (rowFilled[i] == 11) {
            	clearRow(i);
            	++cleared;
            	lastClearedID = blockCounter;
       		}
    	}
	}
    if (cleared) {
        updateCurScoreRow(cleared);
        updateHighScore();
    }
}


void Board::update() {
    if(!updateTheGrid()) return;
    if (atBottom()) {
        updateRowCol();
        gd->updateBlock(loc, color);
        doPrint = false;
        checkFullRow();
    }
	dropped = false;
}



