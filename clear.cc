#include "board.h"
using namespace std;

// Given row r, update 
void Board::clearFromBlockInfo(int r) {
    for (int j=0; j<11; ++j) {
        Cell& c = theGrid[r][j];
        Info& i = blockInfo.find(c.id)->second;
        i.cells_occupied -= 1;
        if (i.cells_occupied == 0) {
            updateCurScoreBlock(i.level_birth);
            blockInfo.erase(c.id);
        }
    }
}

void Board::clearFromRowCol(int r) {
    for (int i=r; i>0; --i) {
        rowFilled[i] = rowFilled[i-1];
        if (rowFilled[i] == 0) {
            maxHeight = i-1;
            break;
        }
    }
    for (int i=0; i<11; ++i) {
        if (18-r < colFilled[i]) --colFilled[i];
        else {
            colFilled[i] = 0;
            for (int j=r+1; j<18; ++j) {
                if (theGrid[j][i].id) {
                    colFilled[i] = 18-j;
                    break;
                }
            }
        }
    }
}

void Board::clearFromTheGrid(int r) {
    for (int i=r; i>maxHeight; --i) {
        for (int j=0; j<11; ++j) {
            theGrid[i][j].id = theGrid[i-1][j].id;
            theGrid[i][j].t = theGrid[i-1][j].t;
            td->notify(theGrid[i][j]);
            gd->notify(theGrid[i][j]);
        }
    }
}

void Board::clearRow(int r) {
    clearFromBlockInfo(r);
    clearFromRowCol(r);
    clearFromTheGrid(r);
}





