#ifndef _TEXT_H_
#define _TEXT_H_

#include <iostream>
#include <vector>
#include <iomanip>
#include "board.h"
#include "eval.h"

using std::vector;
using std::ostream;

class Board;

class TextDisplay {
    vector<vector<char>> theDisplay;
    vector<vector<char>> nextBlock;
public:
    TextDisplay();
    void notify(Cell c);
    void setNext(char t);
    void showBestMove(vector<Pos> hint);

    friend ostream &operator<<(ostream& out, const TextDisplay &td);
};

ostream &operator<<(ostream& out, const TextDisplay &td);

#endif





