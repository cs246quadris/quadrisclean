#include "board.h"
#include <iomanip>
using namespace std;



void Board::goToHint() {
    for (auto &x: loc) {
        //--gridInfo[x.row];
        theGrid[x.row][x.col].id = 0;
        td->notify(theGrid[x.row][x.col]);  // changed from td->clear
        gd->clear(theGrid[x.row][x.col]);
    }
//    for (int i=0; i<4; ++i) {
//        gd->clear(theGrid[loc[i].row][loc[i].col]);
//    }
    anchor = hintAnchor;
    posIndex = bestMovePosInd;
    loc = eval(anchor,posIndex,type);
	dropped = true;
    update();
	dropped = false;
    newBlock();
}



//
//void Board::exec() {
//    while (!gameOver) {
//        hint();
//        goToHint();
//    }
//}
//




// if the drop position contains cell that is already occupied
// check if the id of each cell is not zero->return true
bool Board::invalidPos() {
    
    for (auto x: bestMove) {
        
        if (theGrid[x.row][x.col].id != 0 || x.row >= 18-colFilled[x.col]) {
            return true;
        }
        
    }
    return false;
}

bool Board::emptyBelow() {
    for (auto x: bestMove) {
        if (x.row == 17) { return false;}
        if (theGrid[x.row+1][x.col].id != 0 || x.row+1 >= 18-colFilled[x.col]) {
            return false;
        }
    }
    return true;
}

// drop current block at column col (cursor at column col)
// i is the position index {0,1,2,3}
bool Board::dropPos(int col, int i) {
    // similar to eval
    // nested switch that set bestMove
    int r = 17-colFilled[col];
    int c = col;
    Pos anchor {r,c};
    hintAnchor = anchor;
    bestMove.clear();
    
    bestMove = eval(hintAnchor, i, type);
    
    
    for (auto x: bestMove) {
        if (x.row > 17 || x.row < 0 || x.col > 10 || x.col < 0) {
            return false;
        }
    }
    // if this pos is invalid
    // move it up by 1
    while (invalidPos()) {
        for (auto &x: bestMove) {
            if (x.row >= 3 ) {
                --x.row;
            }
            else { return false; }
        }
        hintAnchor.row--;
    }

    while (emptyBelow()) {
	for (auto &x: bestMove) {
	    if (x.row < 17) {
                ++x.row;
            }
	    else { return false; }
        }
        hintAnchor.row++;
    }    
    return true;
}




void Board::resetCount() {
    for (auto x: bestMove) {
        rowFilled[x.row]++;
        if (18-x.row > colFilled[x.col]) {
            colFilled[x.col] = 18-x.row;
        }
    }
}

int Board::findAG() {
    int sum = 0;
    for (int i = 0; i < 11; i++) {
        sum += colFilled[i];
    }
    return sum;
}



int Board::findCL() {
    int line = 0;
    for (int i = 0; i < 18; i++) {
        if (rowFilled[i] == 11) {
            line++;
        }
    }
    return line;
}

int Board::findH() {
    int holes = 0;
    // for each column, from 18-colFilled to 18
    for (int i = 0; i < 11; i++) {
        for (int j = 18-colFilled[i]; j < 18; j++) {
            if (theGrid[j][i].id == 0) holes++;
        }
    }
    return holes;
}


int Board::findBP() {
    int bumpiness = 0;
    for (int i = 0; i < 10; i++) {
        if (colFilled[i] <= colFilled[i+1]) {
            bumpiness += colFilled[i+1] - colFilled[i];
        }
        else bumpiness += colFilled[i] - colFilled[i+1];
    }
    return bumpiness;
}

int Board::findSH() {
    int topColumn = 0;
    for (int i=0; i < 11; ++i) {
        if (colFilled[i] > topColumn) {
            topColumn = colFilled[i];
        }
    }
    return topColumn;
}


void Board::findBestMove() {
    double maxScore = -1000;
    double currentScore = -1000;
    // AG = aggregate height
    // CL = cleared lines
    // H = holes
    // BP = bumpiness
    // SH = surface height
    int AG, CL, H, BP, SH = 0;
    Type t = type;
    int shapes = numOfShapes(t);
    
    
    vector<int> restoreRowFilled = rowFilled;
    vector<int> restoreColFilled = colFilled;
    bestMove.clear();
    Pos init {0,0};
    for (int i = 0; i < 4; i++) {
        bestMove.emplace_back(init);
    }
    
    hintAnchor = Pos{0,0};
    
    for (int i = 10; i >= 0; i--) {
        for (int pos = 0; pos < shapes; pos++) {
            vector<Pos> restore;
            for (int i = 0; i < 4; i++) {
                restore.emplace_back(bestMove[i]);
            }
            int restorePos = bestMovePosInd;
            bestMovePosInd = pos;
            
            
            Pos restoreAnchor = hintAnchor;
            // set bestMove
            // if the current block of Type t is dropped from i with posInd pos
            if (dropPos(i, pos)){
                // set rowFilled, colFilled
                // by the updated bestMove
                
                resetCount();
                
                AG = findAG();
                CL = findCL();
                H = findH();
                BP = findBP();
                SH = findSH();
                
                currentScore = calculateScore(AG, CL, H, BP, SH);
                if (currentScore < maxScore) {
                    bestMove = restore;
                    bestMovePosInd = restorePos;
                    hintAnchor = restoreAnchor;
                }
                else { maxScore = currentScore; }
                
                rowFilled = restoreRowFilled;
                colFilled = restoreColFilled;
                
            }
            else {
                bestMove = restore;
                bestMovePosInd = restorePos;
                hintAnchor = restoreAnchor;
            }
        }
    }
}

void Board::hint() {
    findBestMove();
    td->showBestMove(bestMove);
    gd->showBestMove(bestMove);
}

void Board::clearhint() {
    for (auto x: bestMove) {
        td->notify(theGrid[x.row][x.col]);
        gd->clear(theGrid[x.row][x.col]);
    }

    //bestMove.clear();
}


bool Board::hintExists() {
    return bestMove.size() == 4;
}




