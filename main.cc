#include <iostream>
#include <sstream>
#include <fstream>
#include "board.h"
#include "welcome.h"
using namespace std;


// kbhit
// test key-being-hit at the current moment
#include <stdio.h>
#include <sys/ioctl.h> // For FIONREAD
#include <termios.h>
#include <stdbool.h>

#include <unistd.h>     // for use of main

int kbhit(void) {
    static bool initflag = false;
    static const int STDIN = 0;
    
    if (!initflag) {
        // Use termios to turn off line buffering
        struct termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initflag = true;
    }
    
    int nbbytes;
    ioctl(STDIN, FIONREAD, &nbbytes);  // 0 is STDIN
    return nbbytes;
}


// Return the index of the first letter in a string
int delimPos(string s) {
    int size = s.length();
    for (int i=0; i<size; ++i) {
        if (!isdigit(s[i])) {
            return i;
        }
    }
    throw "Handle: invalid command";
}

// Read a sequence from given filepath
void readSeq(vector<char>& seq, string path) {
    ifstream file;
    file.open(path.c_str());
    string s;
    seq.clear();
    while (file >> s) 
        seq.emplace_back(s[0]);
}

void readSeq(vector<char>& seq) {
    string path;
    cin >> path;
    ifstream file;
    file.open(path.c_str());
    string s;
    seq.clear();
    while (file >> s) 
        seq.emplace_back(s[0]);
}

bool validCmd(string sub, string s) {
    if (sub.length() == 1 && isupper(sub[0])) return s.find(sub) != string::npos;
    else return (sub == s.substr(0, sub.size()));
}


int main(int argc, char *argv[]) {

    int level = 0;                              
    vector<char> seq;
    string cmd;
    Board board;
    bool wasd = false;
    
    for (int i=1; i<argc; ++i) {
        cmd = argv[i];
        if (cmd == "-text") {
            board.setTextOnly(true);
        } else if (cmd == "-seed") {
            srand(stoi(argv[i+1]));
            ++i;
        } else if (cmd == "-scriptfile") {
            readSeq(seq, argv[i+1]);
            ++i;
        } else if (cmd == "-startlevel") {
            level = stoi(argv[i+1]);
            if (level >= 3) board.setRandom(true);
            ++i;
        }
    }

    /* More error handling here!!! */
    if (level == 0 && seq.size() == 0) {
        vector<char> temp = {'I', 'L', 'O', 'T', 'Z', 'S', 'J'};
        seq = move(temp);
    }
    
    cin.exceptions(ios::eofbit|ios::failbit);
    
    // Testing Welcome Page
    int num = displayWelcome();
    
    // Choose wasd or enter control
    char ctl = chooseControl();
    if (ctl == 'w'){
        wasd = true;
        wasdMan();
    } else enterMan();
    
    board.setClrScheme(num);
    board.setSeq(seq);
    board.setLevel(level);
    board.init();

    cout << board;

    int delim;
    string in = " ";
    int iteration;  // right => iteration = 1; 3right => iteration = 3.
    bool heavy = false;
    char c = ' ';

    while (true) {
// Colour {White, Black, Red, Green, Blue, Cyan, Yellow, Magenta, Orange, Brown};
        if (wasd){
            setbuf(stdin, NULL);
            iteration = 1;
        } else {
            cin >> in;
            delim = delimPos(in);
            iteration = (delim == 0) ? 1 : stoi(in.substr(0, delim));
            cmd = in.substr(delim, in.length());
        }
        bool valid = true;
        
        if (!wasd || kbhit()){
            c = getchar();
            board.clearhint();
        
        if (!wasd && cmd.substr(0, 1) == "n" && validCmd(cmd, "norandom")) {
            board.setRandom(false);
            readSeq(seq);
            board.setSeq(seq);
            board.resetSeqInd();
        } else if (!wasd && cmd.substr(0, 2) == "ra" && validCmd(cmd, "random")) {
            board.setRandom(true);
        } else if (!wasd && cmd.substr(0, 1) == "s" && validCmd(cmd, "sequence")) {
            readSeq(seq);
            board.setSeq(seq);
            board.resetSeqInd();
        }  else if ((wasd && c == 'r')||
                    (!wasd && cmd.substr(0, 2) == "re" && validCmd(cmd, "restart"))) {
            board.restart();
        } else if ((wasd && c == 'h')||
                   (!wasd && cmd.substr(0, 1) == "h" && validCmd(cmd, "hint"))) {
            board.hint();
        } else if (((wasd && c == 'g')||
                   (!wasd && cmd.substr(0, 1) == "g" &&
                    validCmd(cmd, "go"))) && board.hintExists()) {
            board.goToHint();
        } else if ((wasd && c == 'f')||
                   (!wasd && cmd.substr(0,2) == "ex")) {
            while (!board.isGameOver()) {
                board.hint();
                board.goToHint();
                cout << board;
            }
        } else if ((wasd && c == 'q')||
                   (!wasd && cmd.substr(0, 1) == "q" && validCmd(cmd, "quit"))) {
            return 0;  
        } else {
               // look at this
            board.clearOriginal();
            for (int i=0; i<iteration; ++i) {
                if ((wasd && c == 'a')||
                    (!wasd && cmd.substr(0, 3) == "lef" && validCmd(cmd, "left"))) {
                    board.move(Direction::L);
                    heavy = true;
                } else if ((wasd && c == 'd')||
                           (!wasd && cmd.substr(0,2) == "ri" && validCmd(cmd, "right"))) {
                    board.move(Direction::R);
                    heavy = true;
                } else if ((wasd && c == 'z')||
                           (!wasd && cmd.substr(0, 2) == "co" &&
                           validCmd(cmd, "counterclockwise"))) {
                    board.rotate(Rotation::CCW);
                    heavy = true;
                } else if ((wasd && c == 'x')||
                           (!wasd && cmd.substr(0, 2) == "cl" &&
                           validCmd(cmd, "clockwise"))) {
                    board.rotate(Rotation::CW);
                    heavy = true;
                } else if ((wasd && c == 's')||
                           (!wasd && cmd.substr(0, 2) == "do" && validCmd(cmd, "down"))) {
                    board.move(Direction::D);
                } else if ((wasd && c == 'w')||
                           (!wasd && cmd.substr(0, 2) == "dr" && validCmd(cmd, "drop"))) {
                    board.drop();
                } else if ((wasd && c == 'k')||
                           (!wasd && cmd.substr(0, 6) == "levelu" &&
                           validCmd(cmd, "levelup"))) {
                    level = (level < 4 ? (level + 1) : 4);
                    board.setLevel(level);
                } else if ((wasd && c == 'j')||
                           (!wasd && cmd.substr(0, 6) == "leveld" &&
                           validCmd(cmd, "leveldown"))) {
                    level = (level > 0 ? (level - 1) : 0);
                    board.setLevel(level);
                } else if (!wasd && in.length() == 1 && isupper(in[0])) {
                    board.changeType(cmd[0]);
                } else {
                    cout << endl << "Invalid command.\nTry again:" << endl;
                    valid = false;
                    board.addOriginal();
                }
            }
            if (level >= 3 && heavy) {
                board.move(Direction::D);
                heavy = false;
            }
        }
        if(valid){
            board.display();
			// board.debug();
            cout << board;
        }
        if (board.isGameOver()) {
            cout << "Game Over" << endl;
            cout << "Restart?" << endl;
            string s;
            cin >> s;
            if (s == "Yes" || s == "yes" || s == "Y" || s == "y")
                board.restart();
            else
                break;
        }
    }
        else {
            fflush(stdout);
            usleep(50*1000);
        }
    }
}





