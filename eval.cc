#include "board.h"

vector<Pos> eval(Pos pos, int ind, Type t) {
    
    int r = pos.row;
    int c = pos.col;
    int i = ind;
    
    vector<Pos> out;    // what we return

    switch (t) {

        case Type::I:

            switch (i) {

                case 0: // HORIZONTAL
                    out.emplace_back(pos);
                    out.emplace_back(Pos{r, c+1});
                    out.emplace_back(Pos{r, c+2});
                    out.emplace_back(Pos{r, c+3});
                    break;

                case 1: // VERTICAL
                    out.emplace_back(Pos{r-3, c});
                    out.emplace_back(Pos{r-2, c});
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(pos);
                    break;
            }

            break;
    
        case Type::J: 

            switch (i) {


                case 0: //LONG_BOTTOM
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(pos);
                    out.emplace_back(Pos{r, c+1});
                    out.emplace_back(Pos{r, c+2});
                    break;

                case 1: //LONG_LEFT
                    out.emplace_back(Pos{r-2, c});
                    out.emplace_back(Pos{r-2, c+1});
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(pos);
                    break;

                case 2: //LONG_TOP
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(Pos{r-1, c+2});
                    out.emplace_back(Pos{r, c+2});
                    break;
                
                case 3: //LONG_RIGHT
                    out.emplace_back(Pos{r-2, c+1});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(pos);
                    out.emplace_back(Pos{r, c+1});
                    break;
            }
            break;
        
        case Type::L:

            switch (i) {
                    
                case 0: //LONG_BOTTOM
                    out.emplace_back(Pos{r-1, c+2});
                    out.emplace_back(pos);
                    out.emplace_back(Pos{r, c+1});
                    out.emplace_back(Pos{r, c+2});
                    break;
                    
                case 1: //LONG_LEFT
                    out.emplace_back(Pos{r-2, c});
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(pos);
                    out.emplace_back(Pos{r, c+1});
                    break;

                case 2: //LONG_TOP
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(Pos{r-1, c+2});
                    out.emplace_back(pos);
                    break;

                case 3: //LONG_RIGHT
                    out.emplace_back(Pos{r-2, c});
                    out.emplace_back(Pos{r-2, c+1});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(Pos{r, c+1});
                    break;

                
            }
            
            break;
            
        case Type::O: 

            switch (i) {

                case 0: //BASIC
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(pos);
                    out.emplace_back(Pos{r, c+1});
                    break;
            }
            
            break;

        case Type::S: 

            switch (i) {

                case 0: //WIDE
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(Pos{r-1, c+2});
                    out.emplace_back(pos);
                    out.emplace_back(Pos{r, c+1});
                    break;

                case 1: //TALL
                    out.emplace_back(Pos{r-2, c});
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(Pos{r, c+1});
                    break;

            }
            
            break;

        case Type::Z: 

            switch (i) {

                case 0: //WIDE
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(Pos{r, c+1});
                    out.emplace_back(Pos{r, c+2});
                    break;

                case 1: //TALL
                    out.emplace_back(Pos{r-2,c+1});
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(pos);
                    break;

            }
            
            break;

        case Type::T: 

            switch (i) {

                case 0: //DOWN
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(Pos{r-1, c+2});
                    out.emplace_back(Pos{r, c+1});
                    break;

                case 1: //LEFT
                    out.emplace_back(Pos{r-2, c+1});
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(Pos{r, c+1});
                    break;

                case 2: //UP
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(pos);
                    out.emplace_back(Pos{r, c+1});
                    out.emplace_back(Pos{r, c+2});
                    break;

                case 3: //RIGHT
                    out.emplace_back(Pos{r-2, c});
                    out.emplace_back(Pos{r-1, c});
                    out.emplace_back(Pos{r-1, c+1});
                    out.emplace_back(pos);
                    break;
            }
            break;

        default: throw;
    }

    return out;
}




