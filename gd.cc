#include "board.h"

void Board::clearOriginal() {
    for (int i=0; i<4; ++i) {
        gd->clear(theGrid[loc[i].row][loc[i].col]);
    }
    color = typeToColour(type, clrScheme);
}

void Board::display() {
    if (doPrint) gd->updateBlock(loc, color);
    else doPrint = true;
}

void Board::addOriginal() {
    for (int i=0; i<4; ++i) {
        gd->notify(theGrid[loc[i].row][loc[i].col]);
    }
    color = typeToColour(type, clrScheme);
}



