#include "util.h"

Type charToType (char c) {
	switch (c) {
		case 'I': return Type::I;
        case 'J': return Type::J;
        case 'L': return Type::L;
        case 'O': return Type::O;
        case 'S': return Type::S;
        case 'Z': return Type::Z;
		case 'T': return Type::T;
		default: throw "charToType";
    }
}

char typeToChar (Type t) {
	switch (t) {
        case Type::I: return 'I'; break;
        case Type::J: return 'J'; break;
        case Type::L: return 'L'; break;
        case Type::O: return 'O'; break;
        case Type::S: return 'S'; break;
        case Type::Z: return 'Z'; break;
        case Type::T: return 'T'; break;
        case Type::B: return '*'; break;
            
        default:throw;
    }
}

int typeToColour(Type t, int clrScheme) {
    switch (clrScheme){
        case 0:             // Original
            switch (t) {
                case Type::I: return Xwindow::Tomato;
                case Type::J: return Xwindow::DarkOrange;
                case Type::L: return Xwindow::Yellow;
                case Type::O: return Xwindow::YellowGreen;
                case Type::S: return Xwindow::PowderBlue;
                case Type::Z: return Xwindow::CadetBlue;
                case Type::T: return Xwindow::Magenta;
                case Type::B: return Xwindow::Brown;
            }            
        case 1:             //  Night
            return Xwindow::White;        
        case 2:             // Passion
            switch (t) {
                case Type::I: return Xwindow::Tomato;
                case Type::J: return Xwindow::LightSalmon;
                case Type::L: return Xwindow::Yellow;
                case Type::O: return Xwindow::Orange;
                case Type::S: return Xwindow::DarkOrange;
                case Type::Z: return Xwindow::PapayaWhip;
                case Type::T: return Xwindow::Red;
                case Type::B: return Xwindow::Brown;
            }            
        case 3:
            switch (t) {    // Calm
                case Type::I: return Xwindow::YellowGreen;
                case Type::J: return Xwindow::White;
                case Type::L: return Xwindow::Green;
                case Type::O: return Xwindow::PowderBlue;
                case Type::S: return Xwindow::Cyan;
                case Type::Z: return Xwindow::DarkSlateGray;
                case Type::T: return Xwindow::CadetBlue;
                case Type::B: return Xwindow::Brown;
            }
        default:throw;
    }
}

int getBG (int clrScheme){
    switch (clrScheme) {
        case 0: case 2: return Xwindow::Black; break; 
        case 1: case 3: return Xwindow::Grey19; break;
        default:throw;
    }
}

// find number of possible shapes by char
int numOfShapes(Type t) {
    if (t == Type::J || t == Type::L || t == Type::T) return 4;
    else if (t == Type::I || t == Type::Z || t == Type::S) return 2;
    else return 1;
}

// Tetris bot score
double calculateScore (int AG, int CL, int H, int BP, int SH) {
    return -0.50 * AG + 0.75 * CL - 0.85 * H - 0.20 * BP - 0.75 * SH;
}



