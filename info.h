#ifndef _INFO_H_
#define _INFO_H_

#include "type.h"

struct Info {
    int cells_occupied;
    int level_birth;
    Type type;
};

#endif




