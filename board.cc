#include "board.h"
using namespace std;

void Board::setSeq(vector<char> seq) {
    sequence = std::move(seq);
}

void Board::setClrScheme(int num) {
    clrScheme = num;
}

void Board::setRandom(bool r) {
    random = r;
}

void Board::setLevel(int l) {
    level = l;
    if (gd) gd->updateLevel(l);
}

void Board::setTextOnly(bool t) {
    textOnly = t;
}

void Board::resetSeqInd() {
    seqInd = 0;
}

bool Board::isGameOver() {
    return gameOver;
}

void Board::restart() {
    blockCounter = 0;
    curScore = 0;
    gameOver = false;
	lastClearedID = 0;
    blockInfo.clear();
    theGrid.clear();
    rowFilled.clear();
    colFilled.clear();

    delete td;
    delete gd;
    init();
}

void Board::changeType(char t) {
    type = charToType(t);
    color = typeToColour(type,0);
    for (auto x: loc) {
        theGrid[x.row][x.col].id = 0;
        td->notify(theGrid[x.row][x.col]);
        gd->notify(theGrid[x.row][x.col]);
    }
    loc = eval(anchor,0,type);
    update();
}

ostream &operator<<(ostream &out, const Board &b){
    out << endl << "--------------" << endl;
    out << "Level:" << setw(7) << b.level << endl;
    out << "Score:" << setw(7) << b.curScore << endl;
    out << "Hi Score:" << setw(4) << b.hiScore<< endl;
    out << "--------------" << endl;
    out << *(b.td);
    return out;
}



