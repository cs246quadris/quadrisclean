#ifndef _BOARD_H_
#define _BOARD_H_

#include <vector>
#include <map>
#include <iostream>

#include "cell.h"
#include "util.h"
#include "info.h"
#include "text.h"
#include "eval.h"
#include "graphical.h"

using std::vector;
using std::map;
using std::ostream;

class TextDisplay;
class GraphicalDisplay;

class Board {

    // global fields
    int level = 0;
    int blockCounter = 0;
    
    int hiScore = 0;
    int curScore = 0;

    bool random = false;
    bool textOnly = false;
    bool gameOver = false;

    int maxHeight;
	bool dropped;


    // containers
    vector<int> colFilled;
    vector<int> rowFilled;
    vector<vector<Cell>> theGrid;
    map<int, Info> blockInfo;
    vector<char> sequence{'I', 'J', 'L', 'T', 'S', 'Z', 'O'};

    // display
    GraphicalDisplay *gd = nullptr;
    TextDisplay *td = nullptr;

    // graphical
    bool doPrint = true;
    int color = 0;
    int clrScheme = 0;

    // current block fields
    Type type;
    Pos anchor;
    int posIndex;
    vector<Pos> loc;

    // next block fields
    Type nextType = Type::I;
    int seqInd = 0;

    // init.cc
    void initContainers();

    // newblock.cc
    void initNewBlock();
    void validateNewBlock();
    void setNext();

    // action.cc
    bool validLoc(vector<Pos> target);
    void clearBlock(vector<Pos> p);
    
    // clear.cc
    void clearRow(int r);
    void clearFromBlockInfo(int r);
    void clearFromRowCol(int r);
    void clearFromTheGrid(int r);
    void checkFullRow();
    
    // score.cc
    void updateHighScore();
    void updateCurScoreRow(int cleared);
    void updateCurScoreBlock(int level_birth);

    // typeGen.cc
    Type getTypeFromSeq();
    void setNextType();

    // update.cc
    void update();
    bool atBottom();
    bool updateTheGrid();
    void updateRowCol();

    // gd.cc
    
    
    //lvl 4
    int lastClearedID = 1;
    
    // hint.cc
    vector<Pos> bestMove;
    int bestMovePosInd = 0;
    Pos hintAnchor;
    bool invalidPos();
    bool emptyBelow();
    bool dropPos(int col, int i);
    void resetCount();
    int findAG();
    int findCL();
    int findH();
    int findBP();
    int findSH();
    void findBestMove();


    

public:

    // board.cc
    void setClrScheme(int n);
    void setSeq(vector<char> seq);
    void setRandom(bool r);
    void setLevel(int l);
    void setTextOnly(bool t);
    void resetSeqInd();
    void restart();
    bool isGameOver();
    void changeType(char t);

    // action.cc
    bool move(Direction d);
    void rotate(Rotation r);
    void drop();

    // init.cc
    void init();

    // newBlock.cc
    void newBlock();

    // gd.cc
    void clearOriginal();
    void addOriginal();
    void display();
    
    // hint.cc
    void hint();
    void clearhint();
    bool hintExists();
    void goToHint();

    ~Board() { delete td; delete gd; }

    friend ostream &operator<<(ostream &out, const Board &b);
	void debug() {
		for (int i=0; i<11; ++i) {
			std::cout << i << "th col:" << colFilled[i] << std::endl;
		}
		for (int j=0; j<18; ++j) {
			std::cout << j << "th row:" << rowFilled[j] << std::endl;
		}
		for (int i=0; i<18; ++i) {
			for (int j=0; j<11; ++j) {
				if (theGrid[i][j].id == 0) {
					std::cout << ' ';	
				} else {
					std::cout << theGrid[i][j].id;
				}
			}
			std::cout << std::endl;
		}
	}
};

ostream &operator<<(ostream &out, const Board &b);

#endif


        
