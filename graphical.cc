#include "graphical.h"
using namespace std;

GraphicalDisplay::GraphicalDisplay(bool graphicOn, int clrScheme, int lvl, int hiScore): graphicOn{graphicOn}, clrScheme{clrScheme},
window{graphicOn? new Xwindow(cellSize*colNum+bw+bw, cellSize*tr):nullptr} {
    if(!graphicOn) return;
    window->fillRectangle(0,0, cellSize*tc,cellSize*tr, getBG(clrScheme));
    window->drawString(0, (rowNum+4)*(cellSize)+bw,
                     "--------------------------------------------------------------",
                     Xwindow::White);
    window->drawString(0, 4*cellSize,
                       "--------------------------------------------------------------",
                       Xwindow::White);
    window->drawBigString(bw, (rowNum+5)*(cellSize)+3*bw, "Next: ", Xwindow::White);
    window->drawBigString(bw, cellSize, "Level: ", Xwindow::White);
    window->drawBigString(7*cellSize, cellSize, to_string(lvl), Xwindow::White);
    window->drawBigString(bw, 2*cellSize, "Score: ", Xwindow::White);
    window->drawBigString(7*cellSize, 2*cellSize, to_string(0), Xwindow::White);
    window->drawBigString(bw, 3*cellSize, "Hi Score: ", Xwindow::White);
    window->drawBigString(7*cellSize, 3*cellSize, to_string(hiScore), Xwindow::White);
    
    // window->drawBigString(cellSize, 10*cellSize, "  Happy B-Day", Xwindow::White);
    // window->drawBigString(cellSize, 11*cellSize, "My Dearest MOCHI", Xwindow::White);
}

void GraphicalDisplay::notify(Cell c) {
    if(!graphicOn) return;
    if (c.id == 0) {
        // cout << "Clearing " << c.row << " " << c.col << endl;
        window->fillRectangle(bw+cw+c.col*cellSize, (2*cw+c.row)*cellSize+cw,
                              cellSize-2*cw, cellSize-2*cw, getBG(clrScheme));
    } else {
        // cout << "Drawing " << c.row << " " << c.col << endl;
        window->fillRectangle(bw+cw+c.col*cellSize, (2*cw+c.row)*cellSize+cw,
                            cellSize-2*cw, cellSize-2*cw, typeToColour(c.t, clrScheme));
    }
}

void GraphicalDisplay::clear(Cell c) {
    if(!graphicOn) return;
    window->fillRectangle(bw+cw+c.col*cellSize, (2*cw+c.row)*cellSize+cw,
                              cellSize-2*cw, cellSize-2*cw, getBG(clrScheme));
}

// set nextBlock
void GraphicalDisplay::setNext(Type t) {
    if(!graphicOn) return;
    nextBlock.clear();

    for (int i = 0; i < 2; i++) {
        nextBlock.emplace_back(vector<int>{});
        for (int j = 0; j < 4; j++) {
          nextBlock[i].emplace_back(getBG(clrScheme));
        }
    }
    
    int colour = typeToColour(t, clrScheme);       // need to take in clrScheme
    switch(t) {
        case Type::I:
            nextBlock[1][0] = colour;
            nextBlock[1][1] = colour;
            nextBlock[1][2] = colour;
            nextBlock[1][3] = colour;
            break;

        case Type::J:
            nextBlock[0][0] = colour;
            nextBlock[1][0] = colour;
            nextBlock[1][1] = colour;
            nextBlock[1][2] = colour;
            break;

        case Type::L:
            nextBlock[1][0] = colour;
            nextBlock[1][1] = colour;
            nextBlock[1][2] = colour;
            nextBlock[0][2] = colour;
            break;

        case Type::O:
            nextBlock[0][0] = colour;
            nextBlock[0][1] = colour;
            nextBlock[1][0] = colour;
            nextBlock[1][1] = colour;
            break;

        case Type::S:
            nextBlock[0][1] = colour;
            nextBlock[0][2] = colour;
            nextBlock[1][0] = colour;
            nextBlock[1][1] = colour;
            break;

        case Type::Z:
            nextBlock[0][0] = colour;
            nextBlock[0][1] = colour;
            nextBlock[1][1] = colour;
            nextBlock[1][2] = colour;
            break;

        case Type::T:
            nextBlock[0][0] = colour;
            nextBlock[0][1] = colour;
            nextBlock[0][2] = colour;
            nextBlock[1][1] = colour;
            break;

	default:
            break;
    }

    for (int i=0; i < 2; ++i) {
        for (int j=0; j < 4; ++j) {
            window->fillRectangle((4+j)*cellSize+bw + cw, 7*cw+(i+ rowNum + 4)*(cellSize)+bw,
                                  cellSize-2*cw, cellSize-2*cw, nextBlock[i][j]);
        }
    }
}


void GraphicalDisplay::updateBlock(vector<Pos> loc, int curColour) {
    if(!graphicOn) return;
    if (loc.size() != 0){
        for (int i = 0; i < 4; ++i) {
            Pos pos = loc[i];
            // cout << "Drawing(updateBlock) " << pos.row << " " << pos.col << endl;
            window->fillRectangle(bw+cw+pos.col*cellSize, (4+pos.row)*cellSize+cw,
                                  cellSize-2*cw, cellSize-2*cw, curColour);
        }
    }
}


void GraphicalDisplay::updateLevel(int s) {
    window->fillRectangle(7*cellSize, 10, 3*cellSize, cellSize, getBG(clrScheme));
    window->drawBigString(7*cellSize, cellSize, to_string(s), Xwindow::White);
}


void GraphicalDisplay::updateScore(int s) {
    if(!graphicOn) return;
    window->fillRectangle(7*cellSize, cellSize+bw, 3*cellSize, cellSize, 
        getBG(clrScheme));
    window->drawBigString(7*cellSize, cellSize*2, to_string(s), 
        Xwindow::White);
}

void GraphicalDisplay::updateHi(int s) {
    if(!graphicOn) return;
    window->fillRectangle(7*cellSize, 2*cellSize+bw, 3*cellSize, cellSize, 
        getBG(clrScheme));
    window->drawBigString(7*cellSize, 3*cellSize, to_string(s), 
        Xwindow::White);
}

/*
void GraphicalDisplay::displayBestMove(bool b, vector<Pos> loc){
    // if b false: clear bestMove
    int colour = getBG(clrScheme);
    int times = 1;
    
    // b true: display Bestmove in White
    if (b) {
        colour = Xwindow::White;
        times = 4;
    }
    
    for (int i = 0; i < times; ++i){
        for (int i= 0; i < 4;++i) {
            window->fillRectangle(12+loc[i].col*cellSize, (4+loc[i].row)*cellSize+2,
                                  cellSize-4, cellSize-4, colour);
        }
        if (b && colour != Xwindow::White) colour = Xwindow::White;
        else colour = getBG(clrScheme);
    }
}
*/

void GraphicalDisplay::showBestMove(vector<Pos> loc){
    if(!graphicOn) return;
    if (loc.size() != 0){
        for (int i = 0; i < 4; ++i) {
            Pos pos = loc[i];
            // cout << "Drawing(updateBlock) " << pos.row << " " << pos.col << endl;
            window->fillRectangle(bw+cw+pos.col*cellSize, (4+pos.row)*cellSize+cw,
                                  cellSize-2*cw, cellSize-2*cw, Xwindow::White);
            window->fillRectangle(bw+cw+pos.col*cellSize+1, (4+pos.row)*cellSize+cw+1,
                                  cellSize-3*cw, cellSize-3*cw, getBG(clrScheme));
        }
    }
}






