#include "board.h"
using namespace std;


/* Initialize theGrid, rowFilled, colFilled */
void Board::initContainers() {
    for (int i=0; i<18; ++i) {
        theGrid.emplace_back(vector<Cell>{});
        rowFilled.emplace_back(0);
        for (int j=0; j<11; ++j) {
            theGrid[i].emplace_back(Cell{i, j, 0, Type::I});
            colFilled.emplace_back(0);
        }
    }
    td = new TextDisplay();
    if(!textOnly) {
        gd = new GraphicalDisplay(true,clrScheme, level, hiScore);
    }
    else { gd = new GraphicalDisplay(false,0,0,0); }
}

/* Initialize containers and generate a new block */
void Board::init() {
    //cout << "init In" << endl;
    initContainers();
    setNextType();
    newBlock();
    gd->updateBlock(loc, color);
    //cout << "init Out" << endl;
}

