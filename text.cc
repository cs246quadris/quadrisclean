#include "text.h"
using namespace std;

TextDisplay::TextDisplay() {
    //cout << "TextCtor In" << endl;
    for (int i=0; i<18; ++i) {
        theDisplay.emplace_back(vector<char>{});
        for (int j=0; j<11; ++j) {
            theDisplay[i].emplace_back(' ');
        }
    }
    for (int i = 0; i < 2; i++) {
        nextBlock.emplace_back(vector<char>{});
        for (int j = 0; j < 4; j++) {
            nextBlock[i].emplace_back(' ');
        }
    }
    //cout << "TextCtor Out" << endl;
}

void TextDisplay::notify(Cell c) {
    if (c.id == 0) {
        //cout << "Clearing" << c.row << " " << c.col << endl;
        theDisplay[c.row][c.col] = ' ';
    } else {
        //cout << "Updating" << c.row << " " << c.col << endl;
        theDisplay[c.row][c.col] = typeToChar(c.t);
    }
}

void TextDisplay::setNext(char t) {
    //cout << "Text::setNext In" << endl;
    cout << &nextBlock << endl;
    nextBlock.clear();
  for (int i = 0; i < 2; i++) {
    nextBlock.emplace_back(vector<char>{});
    for (int j = 0; j < 4; j++) {
      nextBlock[i].emplace_back(' ');
    }
  }
  switch(t) {
    case 'I':
      nextBlock[1][0] = 'I';
      nextBlock[1][1] = 'I';
      nextBlock[1][2] = 'I';
      nextBlock[1][3] = 'I';
      break;

    case 'J':
      nextBlock[0][0] = 'J';
      nextBlock[1][0] = 'J';
      nextBlock[1][1] = 'J';
      nextBlock[1][2] = 'J';
      break;

    case 'L':
      nextBlock[1][0] = 'L';
      nextBlock[1][1] = 'L';
      nextBlock[1][2] = 'L';
      nextBlock[0][2] = 'L';
      break;

    case 'O':
      nextBlock[0][0] = 'O';
      nextBlock[0][1] = 'O';
      nextBlock[1][0] = 'O';
      nextBlock[1][1] = 'O';
      break;

    case 'S':
      nextBlock[0][1] = 'S';
      nextBlock[0][2] = 'S';
      nextBlock[1][0] = 'S';
      nextBlock[1][1] = 'S';
      break;

    case 'Z':
      nextBlock[0][0] = 'Z';
      nextBlock[0][1] = 'Z';
      nextBlock[1][1] = 'Z';
      nextBlock[1][2] = 'Z';
      break;

    case 'T':
      nextBlock[0][0] = 'T';
      nextBlock[0][1] = 'T';
      nextBlock[0][2] = 'T';
      nextBlock[1][1] = 'T';
      break;
    }
    //cout << "setNext Out" << endl;
}

ostream &operator<<(ostream &out, const TextDisplay &td) {
  for (int i=0; i<18; ++i) {
    for (int j=0; j<11; ++j) {
      out << td.theDisplay[i][j];
    }
    out << endl;
  }

  out << "--------------" << endl;
    out << "Next:" << endl;

  for (int i=0; i < 2; ++i) {
    for (int j=0; j < 4; ++j) {
      out << td.nextBlock[i][j];
    }
    out << endl;
  }
  return out;
}

//=========hint===========
void TextDisplay::showBestMove(vector<Pos> hint) {
    for (auto x: hint) {
        theDisplay[x.row][x.col] = '?';
    }
}





