#include "board.h"
using namespace std;

// Set relevant fields in board
void Board::initNewBlock() {
    type = nextType;
    anchor = Pos{3, 0};
    posIndex = 0;
    blockCounter++;
    /*
    int tempCounter = blockCounter;
    if (level==4 && blockCounter!= lastClearedID
        &&(blockCounter-lastClearedID)%5==0) {++tempCounter;}
    cout << "in init: blockCounter " << tempCounter<< endl;
     */
    blockInfo.insert(make_pair(blockCounter, Info{4, level, type}));
}

// Validata this new block
void Board::validateNewBlock() {
    vector<Pos> target = eval(anchor, posIndex, type);
    for (int i=0; i<4; ++i) {
        // cout << "i is " << i << "  row is" << target[i].row << "  col is" << target[i].col << endl;
        // cout << "findSH is " << findSH() << endl;
        if ((theGrid[target[i].row][target[i].col].id && theGrid[target[i].row][target[i].col].id != blockCounter) ||  findSH() > 17) {
            gameOver = true;
            // means invalid, so gameOver
            return;
        }
    }
    loc = target;
    color = typeToColour(type, clrScheme);
    doPrint = true;
}

// Update everything about next block, including both displays
void Board::setNext() {
    setNextType();
    // cout << (typeToChar(nextType)) << endl;
    td->setNext(typeToChar(nextType));
    gd->setNext(nextType);
}

// main function, handling new block
void Board::newBlock() {
    
    // lvl 4 punish for blocks not cleared after 5+moves
    if (level==4 && blockCounter!= lastClearedID
        &&(blockCounter-lastClearedID)%5==0) {
        
        int r = 17 - colFilled[5];
        if (r < 0) { gameOver = true; return; }
        //place a 1x1 brown block on theGrid[r][5]
        blockCounter++;
        //slastClearedID++;
        blockInfo.insert(make_pair(blockCounter, Info{1, level, Type::B}));
        // update the gridInfo
        ++rowFilled[r];
        ++colFilled[5];
        theGrid[r][5].id = blockCounter;
        theGrid[r][5].t = Type::B;
        // update gd and td
        gd->notify(theGrid[r][5]);
        checkFullRow();
        td->notify(theGrid[r][5]);
    }
    ///------
    
    bestMove.clear();
    initNewBlock();
    validateNewBlock();
    setNext();
    update();
    // printBlock = true;

}




