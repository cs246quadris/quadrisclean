#include <iostream>
#include <string>
#include "welcome.h"

using namespace std;

int displayWelcome(){
    cout << endl;
    cout << "+=============+" << endl;
    cout << "|             |" << endl;
    cout << "|OO           |" << endl;
    cout << "|OO           |" << endl;
    cout << "|             |" << endl;
    cout << "|             |" << endl;
    cout << "|             |" << endl;
    cout << "| WELCOME TO  |" << endl;
    cout << "|     THE     |" << endl;
    cout << "|  GOOD  OLD  |" << endl;
    cout << "|             |" << endl;
    cout << "|Q U A D R I S|" << endl;
    cout << "|             |" << endl;
    cout << "|             |" << endl;
    cout << "|             |" << endl;
    cout << "|            T|" << endl;
    cout << "| I         TT|" << endl;
    cout << "|LI L   J  TST|" << endl;
    cout << "|LI L JJJ TTSS|" << endl;
    cout << "|LL LLZZJOOT S|" << endl;
    cout << "|ZZIT SZZOOIOO|" << endl;
    cout << "|IIIIOOSS  IOO|" << endl;
    cout << "+=============+" << endl;
    cout << " Enter \"0-3\"  To Choose Your Colour Scheme   - >" << endl;
    int num;
    while (true){
        try {
            cin >> num;
            if (num>=0 && num<4) return num;
        } catch ( ... ) {
            cin.clear();
            cin.ignore();
        }
        cout << "Please enter a number between 0 and 3" << endl;
    }
}

char chooseControl(){
    cout << endl << "+=================================================+" << endl;
    cout << " Enter \"w\"  For WASD Control AND Manual Page - >" << endl;
    cout << " Enter \"e\" For ENTER Control AND Manual Page - >" << endl;
    cout << "+=================================================+" << endl << endl;
    string cmd;
    while (true) {
        cin >> cmd;
        if (cmd[0] == 'w' || cmd[0] =='e') return cmd[0];
        cout << "Please enter either w or e" << endl;
    }
    
}

// display WASD CONTROL manual page
void wasdMan(){
    cout << endl;
    cout << "+==================+" << endl;
    cout << "| WASD MANUAL PAGE |" << endl;
    cout << "|                  |" << endl;
    cout << "| r     restart    |" << endl;
    cout << "| h     hint       |" << endl;
    cout << "| g     go to hint |" << endl;
    cout << "| f     auto play  |" << endl;
    cout << "| q     quit       |" << endl;
    cout << "| a     left       |" << endl;
    cout << "| d     right      |" << endl;
    cout << "| z     counter cw |" << endl;
    cout << "| x     clockwise  |" << endl;
    cout << "| s     down       |" << endl;
    cout << "| w     drop       |" << endl;
    cout << "| k     level up   |" << endl;
    cout << "| j     level down |" << endl;
    cout << "|                  |" << endl;
    cout << "|                  |" << endl;
    cout << "|   ENTER ANY KEY  |" << endl;
    cout << "|     TO  QUIT     |" << endl;
    cout << "+==================+" << endl;
    string num;
    cin >> num;
    return;
}

//display ENTER CONTROL manual page
void enterMan(){
    cout << endl;
    cout << "+======================+" << endl;
    cout << "|  ENTER MANUAL PAGE   |" << endl;
    cout << "|                      |" << endl;
    cout << "| n        nonrandom   |" << endl;
    cout << "| ra       random      |" << endl;
    cout << "| s        sequence    |" << endl;
    cout << "| re       restart     |" << endl;
    cout << "| h        hint        |" << endl;
    cout << "| g        go          |" << endl;
    cout << "| ex       autoplay    |" << endl;
    cout << "| q        quit        |" << endl;
    cout << "| lef      left        |" << endl;
    cout << "| ri       right       |" << endl;
    cout << "| co       counter cw  |" << endl;
    cout << "| cl       clockwise   |" << endl;
    cout << "| do       down        |" << endl;
    cout << "| dr       drop        |" << endl;
    cout << "| levelu   level up    |" << endl;
    cout << "| leveld   level down  |" << endl;
    cout << "|                      |" << endl;
    cout << "|                      |" << endl;
    cout << "|                      |" << endl;
    cout << "|     ENTER ANY KEY    |" << endl;
    cout << "|       TO  QUIT       |" << endl;
    cout << "+======================+" << endl;
    string num;
    cin >> num;
    return;
}

/*
char chooseWindow(){
    cout << endl << "+=================================================+" << endl;
    cout << " Enter \"l\"  For Big Window  - >" << endl;
    cout << " Enter \"s\" For Small Window - >" << endl;
    cout << "+=================================================+" << endl << endl;
    string cmd;
    while (true) {
        cin >> cmd;
        if (cmd[0] == 'l' || cmd[0] =='s') return cmd[0];
        cout << "Please enter either w or e" << endl;
    }
    
}
*/





