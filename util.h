#ifndef _UTIL_H_
#define _UTIL_H_

#include "type.h"
#include "window.h"

enum class Rotation {CW, CCW};
enum class Direction {L, R, D};

struct Pos {
    int row;
    int col;
};

Type charToType(char c);
char typeToChar(Type t);
int typeToColour(Type t, int clrScheme);
int getBG(int clrScheme);
int numOfShapes(Type t);
double calculateScore(int AG, int CL, int H, int BP, int SH);

#endif



